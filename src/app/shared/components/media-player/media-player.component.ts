import { Component } from '@angular/core';
import { TrackModel } from '@core/models/tracks.model';

@Component({
  selector: 'mediap-shared',
  templateUrl: './media-player.component.html',
  styleUrl: './media-player.component.css'
})
export class MediaPlayerComponent {
  mockCover: TrackModel = {
    cover: 'https://www.malianteo.com/wp-content/uploads/2023/03/0-42-350x263.jpg',
    name: 'Friends',
    album: 'Luar La L',
    url: 'http://localhost/track.mp3',
    _id: 1,
  }
}
